@extends('layout.index')
@section('judul')
    Tambah Data Baru
@endsection
@section('konten')
<form action="/cast" method="POST">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @csrf
    <div class="form-group">
      <label for="exampleInputEmail1">Nama Cast</label>
      <input type="text" name="nama" class="form-control">
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Umur</label>
      <input type="number" name="umur" class="form-control">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">BIO</label>
      <textarea name="bio" class="form-control" id="" cols="30" rows="10"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection