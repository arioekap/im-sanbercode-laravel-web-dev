@extends('layout.index')
@section('judul')
    Edit Data
@endsection
@section('konten')
<form action="/cast/{{$tampil->id}}" method="POST">
    @method('put')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @csrf
    <div class="form-group">
      <label for="exampleInputEmail1">Nama Cast</label>
      <input type="text" value="{{$tampil->nama}}" name="nama" class="form-control">
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Umur</label>
      <input type="number" value="{{$tampil->umur}}" name="umur" class="form-control">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">BIO</label>
      <textarea name="bio" class="form-control" id="" cols="30" rows="10">{{$tampil->bio}}</textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection