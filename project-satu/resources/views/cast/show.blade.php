@extends('layout.index')
@section('judul')
    Detail Cast
@endsection
@section('konten')

<h2>Show Post {{$tampil->id}}</h2>
<h4>{{$tampil->nama}}</h4>
<p>{{$tampil->umur}}</p>
<p>{{$tampil->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>

@endsection