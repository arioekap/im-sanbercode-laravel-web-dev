@extends('layout.index')
@section('judul')
    Genre
@endsection
@section('konten')

<h2>Show Post {{$tampil->id}}</h2>
<h4>{{$tampil->nama}}</h4>

<div class="row">
@forelse ($tampil->listFilm as $item)
<div class="col-4">
    <div class="card">
        <img src="{{asset('image/'. $item->poster)}}" class="card-img-top" alt="Card image cap">
        <div class="card-body">
          <h3>{{$item->judul}} ({{$item->tahun}})</h3>
          <p class="card-text">{{Str::limit($item->ringkasan, 100)}}</p>
          <a href="/film/{{$item->id}}" class="btn btn-primary btn-block">Detail</a>
        </div>
      </div>
</div>
@empty
    <h4>Tidak Ada Film</h4>
@endforelse
</div>

<a href="/genre" class="btn btn-secondary btn-sm">Kembali</a>

@endsection