@extends('layout.index')
@section('judul')
    Edit Data
@endsection
@section('konten')
<form action="/genre/{{$tampil->id}}" method="POST">
    @method('put')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @csrf
    <div class="form-group">
      <label>Nama Genre</label>
      <input type="text" value="{{$tampil->nama}}" name="nama" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection