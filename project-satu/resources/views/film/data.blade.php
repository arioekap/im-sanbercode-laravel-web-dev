@extends('layout.index')
@section('judul')
    Film
@endsection
@section('konten')
@auth
<a href="/film/create" class="btn btn-primary btn-sm my-3">Tambah</a>   
@endauth
<div class="row">
    @forelse ($films as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{asset('image/'. $item->poster)}}" class="card-img-top" alt="Card image cap">
                <div class="card-body">
                  <h3>{{$item->judul}} ({{$item->tahun}})</h3>
                  <span class="badge badge-info">{{$item->listGenre->nama}}</span>
                  <p class="card-text">{{Str::limit($item->ringkasan, 100)}}</p>
                  <a href="/film/{{$item->id}}" class="btn btn-primary btn-block">Detail</a>
                    @auth
                    <div class="row my-2">
                        <div class="col">
                            <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-block">Edit</a>  
                        </div>
                        <div class="col">
                            <form action="/film/{{$item->id}}" method="POST">
                                @csrf
                                @method("DELETE")
                                <input type="submit" value="Delete" class="btn btn-danger btn-block">
                            </form>    
                        </div>
                    </div>    
                    @endauth
                </div>
              </div>
        </div>
    @empty
        <h4>Kosong</h4>
    @endforelse
</div>

@endsection