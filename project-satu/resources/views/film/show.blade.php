@extends('layout.index')
@section('judul')
    Sinopsis Film
@endsection
@section('konten')
<img src="{{asset('image/'.$films->poster)}}" width="100%" height="600px" alt="">

<h1 class="text-primary">{{$films->judul}} ({{$films->tahun}})</h1>
<p>{{$films->ringkasan}}</p>

<hr>
<h4>List Review</h4>
@forelse ($films->listReview as $item)

<div class="card">
    <div class="card-header">
      {{$item->listUser->name}}
    </div>
    <div class="card-body">
      <p class="card-text">{{$item->content}}</p>
    </div>
</div>
    
@empty
    <h4>Belum ada review</h4>
@endforelse

@auth
    <hr>
    <h4>Tambah Review</h4>
    <form action="/review/{{$films->id}}" method="POST" class="my-3">
        @csrf
        <textarea name="content" id="" cols="30" rows="10" class="form-control" placeholder="Isi Review"></textarea> <br>
        <div class="form-group">
            <label>Point</label>
            <input type="number" name="point" class="form-control">
        </div>
        <input type="submit" class="btn btn-block btn-primary">
    </form>  
@endauth

<a href="/film" class="btn btn-secondary btn-sm">Kembali</a>
@endsection