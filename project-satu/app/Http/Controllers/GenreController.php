<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\models\Genre;

class GenreController extends Controller
{
    public function create(){
        return view ('genre.tambah');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
        ]);

        DB::table('genre')->insert([
            'nama' => $request->input('nama')
        ]);
        return redirect('/genre');
    }

    public function data(){
        $genre = DB::table('genre')->get();
 
        return view('genre.data', ['genre' => $genre]);
    }

    public function show($id){
        $tampil = Genre::find($id);
        return view('genre.show', ['tampil'=>$tampil]);
    }

    public function edit($id){
        $tampil = DB::table('genre')->find($id);
        return view('genre.edit', ['tampil'=>$tampil]);
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
        ]);

        DB::table('genre')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request->input('nama'),
                ]
            );
        return redirect('/genre');
    }

    public function destroy($id){
        DB::table('genre')->where('id', '=', $id)->delete();

        return redirect('/genre');
    }
}
