<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use App\Models\Film;
use Illuminate\Http\Request;
use File;

class FilmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $films = Film::all();
        return view('film.data', ['films' => $films]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $genres = Genre::all();
        return view('film.tambah', ['genres' => $genres]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|min:2',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|mimes:png,jpg,jpeg|max:2048',
            'genre_id' => 'required',
        ]);

        $imageName = time().'.'.$request->poster->extension();
        $request->poster->move(public_path('image'), $imageName);

        Film::create([
            'judul' => $request->input('judul'),
            'ringkasan' => $request->input('ringkasan'),
            'tahun' => $request->input('tahun'),
            'poster' => $imageName,
            'genre_id' => $request->input('genre_id'),
        ]);

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $films=Film::find($id);
        return view('film.show', ["films" => $films]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $films=Film::find($id);
        $genres = Genre::all();

        return view('film.edit', ["films" => $films, "genres" => $genres]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'judul' => 'required|min:2',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'mimes:png,jpg,jpeg|max:2048',
            'genre_id' => 'required',
        ]);

        $films=Film::find($id);

        if ($request->has ('image')) {
            File::delete('image/'. $films->poster);

            $imageName = time().'.'.$request->poster->extension();
            $request->poster->move(public_path('image'), $imageName);

            $films->poster = $imageName;
        }

        $films->judul = $request->input('judul');
        $films->ringkasan = $request->input('ringkasan');
        $films->tahun = $request->input('tahun');
        $films->genre_id = $request->input('genre_id');

        $films->save();

        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $films=Film::find($id);
        File::delete('image/'. $films->poster);
        $films->delete();

        return redirect('/film');
    }
}
