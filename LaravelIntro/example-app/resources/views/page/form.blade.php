<!DOCTYPE html>
<html>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>Full Name:</label><br><br>
        <input type="text" name="name"><br><br>
        <label>Last Name:</label><br><br>
        <input type="text" name="name2"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="0">Male<br>
        <input type="radio" name="gender" value="1">Female<br>
        <input type="radio" name="gender" value="2">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="nation">
            <option value="0">Indonesian</option>
            <option value="1">Singaporean</option>
            <option value="2">Malaysian</option>
            <option value="3">Australian</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bahasa" value="indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="bahasa" value="inggris">English<br>
        <input type="checkbox" name="bahasa" value="other">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea rows="10" cols="30"></textarea><br>
        <input type="submit"><a href="/welcome"></a>
    </form>
</body>
</html>
